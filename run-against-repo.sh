#!/bin/bash -ue

GIT_REPO="$1"
BRANCH="$2"

function show_diff {
    popd || true
    git -C tmp-repo restore .pre-commit-config.yaml
    git -C tmp-repo diff >"$(basename "${GIT_REPO}")"-"${BRANCH}".patch || true
    rm -rf tmp-repo || true
}
trap show_diff ERR
rm -rf tmp-repo || true
git clone --depth=1 --branch "${BRANCH}" "${GIT_REPO}" tmp-repo
pushd tmp-repo

cp ../.pre-commit-config.yaml .

pre-commit install
pre-commit run --all
