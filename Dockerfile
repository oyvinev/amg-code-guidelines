FROM docker:25.0.4

RUN apk add --no-cache --repository=https://dl-cdn.alpinelinux.org/alpine/edge/main musl-dev git python3-dev npm bash && \
    python -m ensurepip --root / && \
    python3 -m pip install --no-cache-dir --break-system-packages pre-commit==3.7.0